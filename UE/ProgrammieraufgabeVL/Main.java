import java.util.*;

public class Main{
	
	static char[][] charArray;
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		int numTestcases = sc.nextInt();
		
		for(int i=0; i<numTestcases; i++) {
			if (isInstance(sc)) {
				System.out.println("YES");
			}else {
				System.out.println("NO");
			}
		}
		
		sc.close();
		
	}
	
	public static boolean isInstance(Scanner sc) {
		
		int height = sc.nextInt();
		int width = sc.nextInt();
		charArray = new char[height][width];
		
		for(int i=0; i<height; i++) {
			String str = sc.next();
			for(int j=0; j<width; j++) {
				charArray[i][j] = str.charAt(j);
			}
		}
		
		printCharArray(charArray);
		
		return true;
	}
	
	/**
	 * prints a character array to the console
	 * @param charArray character Array to be printed
	 */
	public static void printCharArray(char[][] charArray) {
		for(char[] c : charArray) {
			for(char c1 : c) {
				System.out.print(c1);
			}
			System.out.println();
		}
	}
	
	/**
	 * returns true if the tile is black, false otherwise
	 * @param y y position of the tile
	 * @param x x position of the tile
	 * @return true if black, false otherwise
	 */
	public static boolean isBlack(int y, int x) {
		if(charArray[y][x] == 'B') {
			return true;
		}else {
			return false;
		}
	}
	
	/**
	 * returns true if the tile is white, false otherwise
	 * @param y y position of the tile
	 * @param x x position of the tile
	 * @return true if white, false otherwise
	 */
	public static boolean isWhite(int y, int x) {
		if(charArray[y][x] == 'W') {
			return true;
		}else {
			return false;
		}
	}
	
	/**
	 * returns true if the tile is blank, false otherwise
	 * @param y y position of the tile
	 * @param x x position of the tile
	 * @return true if blank, false otherwise
	 */
	public static boolean isBlank(int y, int x) {
		if(charArray[y][x] == '.') {
			return true;
		}else {
			return false;
		}
	}
	
	/**
	 * returns tile at (x,y)
	 * @param y y position of the tile
	 * @param x x position of the tile
	 * @return character at that position
	 */
	public static char getTile(int y, int x) {
		return charArray[y][x];
	}
	
	/**
	 * <Pre>
	 * returns the surrounding Tiles of the tile (t) at the place (x,y)<br>
	 * Indexing of the array as follows:
	 * '+':     0
	 *         3t1
	 *          2
	 * 'o':    701
	 *         6t2
	 *         543
	 * </Pre>
	 * @param y y position of the tile
	 * @param x x position of the tile
	 * @param type '+' for only the adjacent tiles connected with an edge, 'o' for every adjacent tile
	 * @return adjacent tiles
	 */
	public static char[] surroundingTiles(int y, int x, char type) {
		char[] cA;
		switch (type) {
			case '+':
				cA = new char[4];
				for(int i=0; i<cA.length; i++) {
					try {
						switch (i) {
						case 0:
							cA[i] = getTile(y-1, x);
							break;
						case 1:
							cA[i] = getTile(y, x+1);
							break;
						case 2:
							cA[i] = getTile(y+1, x);
							break;
						case 3:
							cA[i] = getTile(y, x-1);
							break;
						}
					} catch (java.lang.ArrayIndexOutOfBoundsException e) {
						cA[i] = '.';
					}
				}
			case 'o':
				cA = new char[8];
				for(int i=0; i<cA.length; i++) {
					try {
						switch (i) {
						case 0:
							cA[i] = getTile(y-1, x);
							break;
						case 1:
							cA[i] = getTile(y-1, x+1);
							break;
						case 2:
							cA[i] = getTile(y, x+1);
							break;
						case 3:
							cA[i] = getTile(y+1, x+1);
							break;
						case 4:
							cA[i] = getTile(y+1, x);
							break;
						case 5:
							cA[i] = getTile(y+1, x-1);
							break;
						case 6:
							cA[i] = getTile(y, x-1);
							break;
						case 7:
							cA[i] = getTile(y-1, x-1);
							break;
						}
					} catch (java.lang.ArrayIndexOutOfBoundsException e) {
						cA[i] = '.';
					}
				}
				
			default:
				cA = new char[1];
				System.out.println("Ung�ltiger Aufruf f�r surroundingTiles");
		}
		
		return cA;
		
	}
	
}