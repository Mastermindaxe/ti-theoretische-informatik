package automata;
public class Edge {
	
	/**
	 * start state of the edge
	 */
	private int from;
	
	/**
	 * end state of the edge
	 */
	private int to;
	
	/**
	 * symbol to transition with
	 */
	private char transition;
	
	
	/**
	 * Takes an integer to be the starting point of the edge, a character as the symbol 
	 * to transition with and the end point for the edge
	 * @param from starting point (int)
	 * @param trans transition (char)
	 * @param to end point (int)
	 */
	public Edge(int from, char trans, int to){
		this.from = from;
		this.transition = trans;
		this.to = to;
	}
	
	
	/**
	 * setter for the starting point
	 * @param to starting point (int)
	 */
	public void setTo(int to){
		this.to = to;
	}
	
	/**
	 * getter for the destination
	 * @return destination of the edge (int)
	 */
	public int getTo(){
		return to;
	}
	
	/**
	 * setter for the starting point of the edge
	 * @param from starting point (int)
	 */
	public void setFrom(int from){
		this.from = from;
	}
	
	/**
	 * getter for the starting point
	 * @return starting point (int)
	 */
	public int getFrom(){
		return from;
	}
	
	/**
	 * setter for the symbol to transition with
	 * @param trans transitioning symbol
	 */
	public void setTransition(char trans){
		this.transition = trans;
	}
	
	/**
	 * getter for the symbol with which the transition is made
	 * @return symbol of the transition (char)
	 */
	public char getTransition(){
		return transition;
	}
	
	/**
	 * returns the origin + transition + destination of the transition as a
	 * concatenated string
	 */
	@Override
	public String toString() {
		return ( String.valueOf(getFrom()) + " " + Character.toString(getTransition()) + " " + String.valueOf(getTo()) );
	}
	
}
