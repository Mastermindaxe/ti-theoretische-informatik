package automata;
public class Automata {
	
	/** 
	 * integer array for saving the states of the automata e.q. (q_0, q_1)
	 */
	int[] states;
	
	/**
	 * integer array for the accepting states, e.q. (q_F)
	 */
	int[] acceptingStates;
	
	/**
	 * the alphabet of the automata
	 */
	char[] alphabet;
	
	
	/**
	 * edges of the automata, e.q. (from 0 to 4 with a)
	 */
	Edge[] edges;
	
	/**
	 * variable to determine if the input/automata is valid
	 */
	boolean valid = true;
	
	/**
	 * Constructor to take three arguments
	 * @param alphabets |Q|,|F|,|Sigma|
	 * @param acceptingStates accepting states of the automata
	 * @param alphabet the alphabet of the automata
	 */
	public Automata( int[] alphabets, int[] acceptingStates, char[] alphabet ){
		// test if alphabets contains 3 numbers
		if (alphabets.length != 3) {
			valid = false;
		}
		
		// test for valid values for the lengths of the alphabets
		for ( int n : alphabets ){
			if (n < 1 || n > 10) {
				valid = false;
			}
		}
		
		// define the amount of states
		this.states = new int[ alphabets[0] ];
		
		for( int i=0; i<states.length; i++ ){
		    states[i] = i;
		}
		
		// define the accepting states
		this.acceptingStates = new int[ alphabets[1] ];
		this.acceptingStates = acceptingStates;
		
		// define the alphabet for the edges
		this.alphabet = alphabet;
		
		edges = new Edge[this.states.length * this.alphabet.length];
	}
	
	public int[] getStates() {
		return states;
	}
	
	public int[] getAcceptingStates() {
		return acceptingStates;
	}
	
	public char[] getAlphabet() {
		return alphabet;
	}
	
	public Edge[] getEdges() {
		return edges;
	}
	
	/**
	 * adds an edge to the edges array if space is available
	 * @param from starting state of the edge
	 * @param with symbol from the alphabet to transition with
	 * @param to end state of the edge
	 */
	public void addEdge(int from, char with, int to) {
		for ( int i=0; i<edges.length; i++ ) {
			if (edges[i] == null) {
				edges[i] = new Edge(from, with, to);
				break;
			}
		}
	}
	
	/**
	 * tests if the automata is valid
	 */
	public void evaluate() {
		int[] visited = new int[states.length];
		
		valid = evaluateFrom(0, visited);
	}
	
	/**
	 * checks if one of the accepting states is reachable from a state through recursion. Also uses an array called visited which holds information
	 * on which states were already visited, to avoid being stuck in an endless loop
	 * @param stateFrom state to start with
	 * @param visited array which holds the already visited states
	 * @return boolean value which indicates if an accepting state is reachable or not
	 */
	public boolean evaluateFrom(int stateFrom, int[] visited) {
		boolean output = false;
		
		// terminates if the state was already visited
		if( visited[stateFrom] == 1 ) {
			return false;
		}
		else {
			visited[stateFrom] = 1;
			
			// iterates through the accepting states
			for(int acceptingState : acceptingStates) {
				// returns true if the current state is an accepting one
				if( stateFrom == acceptingState ) {
					return true;
				}
				// checks reachability of the current accepting state for all available transitions
				for( char c : alphabet) {
					if( evaluateFrom( getEdge(stateFrom, c).getTo() , visited) ) {
						// doesn't immediately return to avoid only checking one transition
						// thus writing to a variable, because we only want to know if an accepted state is reachable at all, not which and how often
						output = true;
					}
				}
			}
		}
		
		return output;
		
	}
	
	/*public boolean reachesAcceptingState(int stateFrom, char transition) {
		for( int acceptingState : acceptingStates) {
			if( getEdge(stateFrom, transition).getTo() == acceptingState) {
				return true;
			}
		}
		return false;
	}*/
	
	/**
	 * returns an edge from two given parameters, which are the origin from which the edge is orginating and the transition it uses
	 * @param stateFrom origin of the edge
	 * @param transition symbol of the alphabet to be used
	 * @return returns an Edge with given paramters
	 */
	public Edge getEdge(int stateFrom, char transition) {
		for( int i=0; i<alphabet.length; i++ ) {
			if( transition == alphabet[i] ) {
				return edges[ stateFrom*alphabet.length + i ];
			}
		}
		return null;
	}
	
	/**
	 * indicates whether the automata is a valid one, or not
	 * @return valid
	 */
	public boolean isValid() {
		return valid;
	}
	
	/**
	 * prints out the states, accepting states and the alphabet
	 * separated by a comma
	 */
	@Override
	public String toString() {
		String out = "";
		
		for( int i : this.states ) {
			out = out + String.valueOf(i) + " "; 
		}
		
		out = out + "\n";
		
		for( int i : this.acceptingStates ) {
			out = out + String.valueOf(i) + " ";
		}
		
		out = out + "\n";
		
		for( char ch : this.alphabet ) {
			out = out + Character.toString(ch) + " ";
		}
		
		out = out + "\n";
		
		for( Edge edge : this.edges ) {
			out = out + edge.toString() + "\n";
		}
		
		return out;
	}
	
}
