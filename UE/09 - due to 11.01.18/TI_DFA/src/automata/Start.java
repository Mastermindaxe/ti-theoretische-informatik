package automata;
import java.util.Scanner;

public class Start {
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		// read the first line of the input and write that into alphabets
		int[] alphabets = { sc.nextInt(), sc.nextInt(), sc.nextInt() };
		
		// read the next line containing the accepting states
		int[] acceptingStates = new int[ alphabets[1] ];
		for( int i=0; i < acceptingStates.length; i++ ) {
			acceptingStates[i] = sc.nextInt();
		}
		
		// read the next line containing the alphabet
		char[] alphabet = new char[ alphabets[2] ];
		for( int i=0; i < alphabet.length; i++ ) {
			alphabet[i] = sc.next().charAt(0);
		}
		
		// construct the automata with the given parameters
		Automata auto = new Automata(
				alphabets, 
				acceptingStates, 
				alphabet);
		
		// read the transitions
		int maxIter = (auto.getStates().length * auto.getAlphabet().length);
		for (int i=0; i<maxIter; i++) {
			auto.addEdge(sc.nextInt(), sc.next().charAt(0), sc.nextInt());
		}
		
		// close the scanner
		sc.close();
		
		// evaluate the automata to check if it is a valid one
		auto.evaluate();
		
		if( auto.isValid() ) {
			System.out.println("verzogen");
		} else {
			System.out.println("fehlerhaft");
		}

	}

}
